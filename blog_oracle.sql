
CREATE TABLE article
(
    id number(11) ,
    title VARCHAR2(100) NOT NULL ,
    content clob ,
    createDate date DEFAULT sysdate NOT NULL ,
    stateDate date DEFAULT sysdate NOT NULL,
    brief clob ,
    tag_id number(11) NOT NULL ,
    hits number(11) DEFAULT '0' NOT NULL ,
    bad number(11) DEFAULT '0' NOT NULL ,
    good number(11) DEFAULT '0' NOT NULL ,
    image VARCHAR2(255) DEFAULT '' ,
    CONSTRAINT article_pk PRIMARY KEY(id)  
);
comment on column article.title is '文章标题';
comment on column article.content is '文章内容';
comment on column article.createDate is '存档时间';
comment on column article.stateDate is '存档时间';
comment on column article.brief is '简介';
comment on column article.tag_id is '分类id';
comment on column article.hits is '点击次数';
comment on column article.bad is '不好';
comment on column article.good is '点赞';
comment on column article.image is '文章logo';

CREATE TABLE resources
(
    id number(11) ,
    name VARCHAR2(255) ,
    brief clob ,
    links VARCHAR2(255)  ,
    createDate date DEFAULT sysdate NOT NULL ,
    stateDate date DEFAULT sysdate NOT NULL,
    downnum number(11) DEFAULT '0',
    CONSTRAINT resource_pk PRIMARY KEY(id)
);
comment on column resources.name is '资源名称';
comment on column resources.brief is '资源简介';
comment on column resources.links is '资源链接';
comment on column resources.createDate is '存档时间';
comment on column resources.stateDate is '存档时间';
comment on column resources.downnum is '下载次数';

CREATE TABLE tags
(
    id number(11) ,
    tagname VARCHAR2(30) DEFAULT '' NOT NULL ,
    logo VARCHAR2(255) DEFAULT '' ,
    created_at date,
    CONSTRAINT tags_pk PRIMARY KEY(id)
);

comment on column tags.tagname is '分类名称';
comment on column tags.logo is 'tag的图片';
comment on column tags.created_at is '创建时间';