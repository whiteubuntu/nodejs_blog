let express = require('express');
let path = require('path');

let favicon = require('serve-favicon');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');

let routes = require('./controllers/index');
let users = require('./controllers/users');
let admin = require('./controllers/admin');
let tag = require('./controllers/tag');
let article = require('./controllers/article');
var app = express();
let resource = require('./controllers/resource');
var resourceDao = require('./services/Resource');
var multer  = require('multer')

// view engine setup
let dirname=__dirname;

let absoluteServePath='./uploads';
let downloadLinks='http://localhost:3000/download';


app.post('/profile', function (req, res, next) {
    let storage=multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, absoluteServePath)
        },
        filename:function (req,file,cb) {
            cb(null,file.originalname);
        }
    });
    // var upload=multer({storage:storage}).single('avatar');
    var upload=multer({storage:storage}).single('avatar');
    upload(req,res,function (err) {
        if(err) {
            console.log(err);
            return res.json({status : 0,msg:'更新失败'});
        }else {
            console.log(req.body);
            var brief=req.body.brief;
            var id=req.body.id;
            var name=req.body.name;
            if(req.file){
                name=req.file.originalname;
            }
            var links=downloadLinks+"/"+id;
            resourceDao.resourceUpdate(id,name,brief,links,function(){
                res.json({status : 1,msg:'更新成功'});
            });
        }

    })

})




app.set('views', path.join(dirname, 'views'));
//设置模板的后缀是html
app.engine('html', require('ejs').renderFile);
//指定总模板
app.set('view engine', 'html');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(dirname, 'public')));

app.use('/', routes);
app.use('/users', users);
app.use('/admin', admin);
app.use('/tag', tag);
app.use('/article', article);
app.use('/resource', resource);



// catch 404 and forward to error handler
app.use(function (req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
};

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});
module.exports = app;
