DROP TABLE IF EXISTS `article`;
CREATE TABLE article
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    title VARCHAR(100) NOT NULL COMMENT '文章标题',
    content TEXT COMMENT '文章内容',
    pubtime DATETIME DEFAULT '2020-11-01 00:00:00' NOT NULL COMMENT '发布时间',
    date VARCHAR(20) COMMENT '存档时间',
    brief TEXT COMMENT '简介',
    tag_id TINYINT(11) NOT NULL COMMENT '分类id',
    hits INT(11) DEFAULT '0' NOT NULL COMMENT '点击次数',
    bad INT(11) DEFAULT '0' NOT NULL COMMENT '不好',
    good INT(11) DEFAULT '0' NOT NULL COMMENT '点赞',
    image VARCHAR(255) DEFAULT '' COMMENT '文章logo'
);
DROP TABLE IF EXISTS `resource`;
CREATE TABLE resource
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) COMMENT '资源名称',
    brief TEXT COMMENT '资源简介 ',
    links VARCHAR(255) COMMENT '资源链接',
    pubtime DATETIME,
    downnum INT(11) DEFAULT '0'
);
DROP TABLE IF EXISTS `tags`;
CREATE TABLE tags
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    tagname VARCHAR(30) DEFAULT '' NOT NULL COMMENT '分类名称',
    logo VARCHAR(255) DEFAULT '' COMMENT 'tag的图片',
    created_at TIMESTAMP
);