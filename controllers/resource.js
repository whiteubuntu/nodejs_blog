var express = require('express');
var router = express.Router();
var moment = require('moment');
var multer  = require('multer')
var resourceDao = require('../services/Resource');
//添加文章
router.get('/add', function (req, res, next) {
    res.render('admin/backend/resource/addresource', { path: '/resource/add', open: 'resource'});
});

let absoluteServePath='./uploads';


//添加文章处理
router.post('/addresource', function (req, res, next) {
    let storage=multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, absoluteServePath)
        },
        filename:function (req,file,cb) {
            cb(null,file.originalname);
        }
    });
    var upload=multer({storage:storage}).single('avatar');
    upload(req,res,function (err) {
        var name = req.body.name;
        if(req.file){
            name=req.file.originalname;
        }
        var links="";
        var brief = req.body.brief;
        resourceDao.addResource(name, brief, links, function (err, results) {
            if (err) {
                res.json({status: 0, msg: '添加失败'});
            }
            res.json({status: 1, msg: '添加成功'});
        });
    });

});
//文章列表
router.get('/resources', function (req, res, next) {

    //每页显示的记录数
    var pageSizes = 10;
    //当前页
    var pageNow = req.query.page ? req.query.page : 1;
    //获取总文章数量
    resourceDao.resourceCount(function (err, count) {
        //获取文章列表
        resourceDao.resouceList(pageSizes, pageNow, function (err, resourceList) {

            if (err) {

                return next(err);
            }
            for (var i = 0; i < resourceList.length; i++) {
                resourceList[i].pubtime = moment(resourceList[i].pubtime).format('YYYY-MM-DD HH:mm:ss');
                console.log(resourceList[i].tag);
            }
            //计算总页数
            var totalPage = parseInt((count + pageSizes - 1) / pageSizes);

            res.render('admin/backend/resource/index', {
                resourceList: resourceList,
                totalCount: count,
                totalPage: totalPage,
                currentPage: pageNow,
                path: '/resource/resources',
                open: 'resource'
            });
        });
    });
});
//根据文章id查询文章
router.get('/queryById', function (req, res, next) {

    var id = req.query.id;
    resourceDao.resourceById(id, function (err, resource) {

        if (err) {

            return next(err);
        }
        resource[0].pubtime = moment(resource[0].pubtime).format('YYYY年MM月DD日');
        res.json({resource: resource[0]})
    });
});
//更新文章
router.get('/updateresource', function (req, res, next) {

    var id = req.query.id;

    resourceDao.resourceById(id, function (err, resource) {
        res.render('admin/backend/resource/update', {
            resource: resource[0],
            path: '/resource/resources',
            open: 'resource'
        });
    });
});
//删除文章
router.get('/deleteById', function (req, res, next) {

    var id = req.query.id;

    resourceDao.resourceById(id, function (err, result) {
        if (err) {
            return next(err);
        }
        result[0].remove(function (err) {
            if(err){
                res.json({status: 0, msg: '失败删除'});
            }else {
                res.json({status: 1, msg: '删除成功'});
            }
        })
    });
});
//更新文章处理
router.post('/updateresourcedone', function (req, res, next) {

    var id = req.body.id;
    var name = req.body.name;
    var links = req.body.links;
    var brief = req.body.brief;
    resourceDao.resourceUpdate(id, name, brief,links, function (err, result) {
        if (err) {
            res.json({status: 0, msg: '更新失败'});
        }
        res.json({status: 1, msg: '更新成功'});
    });
});

//上传文件
router.post('/uploadfile', function (req, res, next) {
    let file = req.files.logo;
    console.log('文件类型：%s', file.type);
    console.log('原始文件名：%s', file.name);
    console.log('文件大小：%s', file.size);
    console.log('文件保存路径：%s', file.path);
});

module.exports = router;