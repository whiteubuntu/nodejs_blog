let db = require('./mysql');
let resource = {
    id: { type: 'serial', key: true } , //主键
    name : String,
    pubtime : {type: 'date',time: true },
    brief : {type : 'text'},
    links : {type : 'text'},
    downnum : {type: 'integer',defaultValue: 0 }
};

let Resource = db.define('resource',resource);

module.exports = Resource;